const Joi = require("@hapi/joi");

const PAGINATION = {
  limit: Joi.number().integer().positive().default(10),
  offset: Joi.number().integer().positive().allow(0),
};

module.exports = {
  USER: {
    ROLE: {
      USER: "USER",
      ADMIN: "ADMIN",
    },
  },
  EVENTS: {
    ON_POST_PUBLISH: "ON_POST_PUBLISH",
    ON_DONE_PUBLISH: "ON_DONE_PUBLISH",
  },
  LIVE_STATUS: {
    PENDING: "PENDING",
    STREAMING: "STREAMING",
    STREAMING_DONE: "STREAMING_DONE",
  },
  SCHEMA: {
    PAGINATION,
  },
  DEFAULT_PAGINATION_LIMIT: 10,
  STREAMING_PREFIX: "/cloud/public/live",
  STREAMING_STATIC_PREFIX: "/cloud/public/storage/streams",
};

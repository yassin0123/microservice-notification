module.exports = {
  LOG_LEVEL: process.env.LOG_LEVEL || "info",
  HOST: process.env.HOST || "0.0.0.0",
  PORT: process.env.PORT || 8080,
  DB: "Psycho",
  DB_URL_TEMPLATE:
    process.env.DB_URL_TEMPLATE || "mongodb://{{dbHost}}:27017/{{db}}",
  AUTH_SECRET: "thisisverrysecret",
};

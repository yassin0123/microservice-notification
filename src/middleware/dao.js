const _ = require("lodash");
const createError = require("http-errors");
const dbProvider =  require("../db");
const { ObjectId } = require("mongodb");
const serialize = require("../services/serializer");
const upsert = require("../services/upsert");

module.exports = function (Model) {
  if (!Model) {
    throw createError.BadRequest("dao error, no Model is specified");
  }

  this.serializeGroup = ({ group }) => async (req, res, next) => {
    req.render = ObjectId.isValid(req.params._id)
      ? await serialize(req, {
          Model,
          query: req.query,
          options: {
            group: group || "details",
            refs: ObjectId(req.params._id),
          },
        })
      : await serialize(req, {
          Model,
          query: req.query,
          options: { group: group || "simple" },
        });

    return next();
  };

  this.upsert = async (req, res, next) => {
    if (req.params._id) {
      req.body._id = req.params._id;
    }
    const _id = await upsert({ Model, payload: req.body });
    req.render = req.params._id ? { Model } : { Model, _id };
    return next();
  };

  this.all = this.serializeGroup({ group: "simple" });
  this.details = this.serializeGroup({ group: "details" });

  this.delete = async (req, res, next) => {
    const db = await dbProvider.get();
    await db
      .collection(Model.collection)
      .deleteOne({ _id: ObjectId(req.params._id) });
    return next();
  };
};

const _ = require("lodash");
const Model = require("../models/user");
const createError = require("http-errors");
const { ObjectId } = require("mongodb");
const dbProvider = require("../db");
const { AUTH_SECRET } = require("../config");
const jwt = require("jsonwebtoken");

exports.check = async (req, res, next) => {
  var token = req.headers["token"];
  if (!token) {
    return next();
  }

  jwt.verify(token, AUTH_SECRET, async function (err, decoded) {
    if (err) {
      return next();
    }

    const { _id, role } = decoded;
    const db = await dbProvider.get();
    const exists = await db
      .collection(Model.collection)
      .findOne({ _id: ObjectId(_id) }, { projection: { _id: 1, role: 1 } });

    if (!exists) {
      return next();
    }

    req.auth = {
      _id,
      role,
      token,
    };
    return next();
  });
};

exports.isUser = async (req, res, next) => {
  if (!req.auth) {
    throw new createError.Unauthorized("Unauthorized operation");
  }
  return next();
};

exports.isAdmin = async (req, res, next) => {
  if (!_.get(req, "auth.role", null) === "ADMIN") {
    throw new createError.Unauthorized("Unauthorized operation");
  }
  return next();
};

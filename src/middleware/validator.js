const _ = require("lodash");
const Promise = require("bluebird");
const createError = require("http-errors");
const { ObjectID, ObjectId } = require("mongodb");
const dbProvider =  require("../db");

const {
  getInvalidIdMessage,
  getNonExistantIdMessage,
} = require("../templates/validation");

const validateSchema = ({ schema, payload }) => {
  if (!schema) {
    return;
  }

  const { error, value } = schema.validate(payload);
  if (error) {
    throw createError.BadRequest(error.message);
  }

  return value;
};

const validateIdAsync = async (Model, { _id }) => {
  if (!ObjectID.isValid(_id)) {
    throw createError.BadRequest(getInvalidIdMessage(_id));
  }

  const db = await dbProvider.get();
  const count = await db
    .collection(Model.collection)
    .countDocuments({ _id: new ObjectID(_id) });
  if (count === 0) {
    throw createError.BadRequest(getNonExistantIdMessage(_id));
  }
};

const validateIdsAsync = async (validations) => {
  if (_.isEmpty(validations)) {
    return;
  }

  for (const _id of _(validations).map("ids").flattenDeep().value()) {
    if (!ObjectID.isValid(_id)) {
      throw createError.BadRequest(getInvalidIdMessage(_id));
    }
  }
  const db = await dbProvider.get();
  await Promise.each(validations, async ({ model: { collection }, ids }) => {
    const mongoIds = ids.map(ObjectId);
    const items = await db
      .collection(collection)
      .find({ _id: { $in: mongoIds } }, { projection: { _id: 1 } })
      .toArray();

    if (_.size(items) !== _.size(ids)) {
      throw createError.BadRequest(
        getNonExistantIdMessage(
          _.chain(ids)
            .difference(items.map((it) => it._id.toString()))
            .head()
        )
      );
    }
  });
};

const validateAsync = async ({ validation }, req) =>
  _.isArray(validation)
    ? await Promise.each(validation, async (fn) => await fn(req))
    : await validation(req);

module.exports = function (Model, validations = {}) {
  if (!Model) {
    throw createError.BadRequest("no model provided");
  }

  const { post, put, get, delete: remove, extras } = validations;

  this.post = async (req, res, next) => {
    req.body = validateSchema({
      schema: Model.schema.upsert || Model.schema.post,
      payload: req.body,
    });

    post && (await validateAsync({ validation: post }, req));
    req.validateIdsAsync && (await validateIdsAsync(req.validateIdsAsync));

    return next();
  };

  this.put = async (req, res, next) => {
    req.body = validateSchema({
      schema: Model.schema.upsert || Model.schema.put,
      payload: req.body,
    });

    await validateIdAsync(Model, { _id: req.params._id });
    put && (await validateAsync({ validation: put }, req, res, next));
    req.validateIdsAsync && (await validateIdsAsync(req.validateIdsAsync));

    return next();
  };

  this.patch = async (req, res, next) => {
    req.body = validateSchema({
      schema: Model.schema.patch || Model.schema.upsert,
      payload: req.body,
    });

    await validateIdAsync(Model, { _id: req.params._id });
    put && (await validateAsync({ validation: put }, req, res, next));
    req.validateIdsAsync && (await validateIdsAsync(req.validateIdsAsync));

    return next();
  };

  this.get = async (req, res, next) => {
    req.query = validateSchema({
      schema: _.get(
        Model.schema,
        req.params._id ? "route.single" : "route.all",
        null
      ),
      payload: req.query,
    });
    req.params._id && (await validateIdAsync(Model, { _id: req.params._id }));
    get && (await validateAsync({ validation: get }, req));
    req.validateIdsAsync && (await validateIdsAsync(req.validateIdsAsync));

    return next();
  };

  this.delete = async (req, res, next) => {
    await validateIdAsync(Model, { _id: req.params._id });
    remove && (await validateAsync({ validation: remove }, req));
    req.validateIdsAsync && (await validateIdsAsync(req.validateIdsAsync));

    return next();
  };

  this.extras = extras;
};

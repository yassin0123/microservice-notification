const _ = require("lodash");
const dbProvider =  require("../db");
const { ObjectId } = require("mongodb");
const Promise = require("bluebird");
const createError = require("http-errors");

exports.React = function (
  Model,
  withUpDown = { up: "liked", down: "disliked" }
) {
  if (!Model) {
    throw createError.BadRequest("social error, no Model is specified");
  }

  this.applyUp = async ({ _id, userId }) => {
    const db = await dbProvider.get();
    [_id, userId] = [ObjectId(_id), ObjectId(userId)];
    const component = await db.collection(Model.collection).findOne(
      {
        _id,
        $or: [{ [withUpDown.up]: userId }, { [withUpDown.down]: userId }],
      },
      { projection: { [withUpDown.up]: 1, [withUpDown.down]: 1 } }
    );

    if (component) {
      const didUp = _.chain(component)
        .get(`${withUpDown.up}`, [])
        .map((it) => it.toString())
        .find((it) => it == userId.toString())
        .value();
      if (didUp) {
        const {
          value: { [withUpDown.up]: upList },
        } = await db
          .collection(Model.collection)
          .findOneAndUpdate(
            { _id },
            { $pull: { [withUpDown.up]: userId } },
            { projection: { [withUpDown.up]: 1 }, returnOriginal: false }
          );
        return {
          [withUpDown.up]: {
            count: _.size(upList),
            used: false,
          },
        };
      }
      const {
        value: { [withUpDown.up]: upList, [withUpDown.down]: downList },
      } = await db.collection(Model.collection).findOneAndUpdate(
        { _id },
        {
          $push: { [withUpDown.up]: userId },
          $pull: { [withUpDown.down]: userId },
        },
        {
          projection: { [withUpDown.up]: 1, [withUpDown.down]: 1 },
          returnOriginal: false,
        }
      );
      return {
        [withUpDown.up]: {
          count: _.size(upList),
          used: true,
        },
        [withUpDown.down]: {
          count: _.size(downList),
          used: false,
        },
      };
    }

    const {
      value: { [withUpDown.up]: upList },
    } = await db.collection(Model.collection).findOneAndUpdate(
      { _id },
      { $push: { [withUpDown.up]: userId } },
      {
        projection: { [withUpDown.up]: 1 },
        returnOriginal: false,
      }
    );

    return {
      [withUpDown.up]: {
        count: _.size(upList),
        used: true,
      },
    };
  };

  this.applyDown = async ({ _id, userId }) => {
    const db = await dbProvider.get();
    [_id, userId] = [ObjectId(_id), ObjectId(userId)];
    const component = await db.collection(Model.collection).findOne(
      {
        _id,
        $or: [{ [withUpDown.up]: userId }, { [withUpDown.down]: userId }],
      },
      { projection: { [withUpDown.up]: 1, [withUpDown.down]: 1 } }
    );

    if (component) {
      const didDown = _.chain(component)
        .get(`${withUpDown.down}`, [])
        .map((it) => it.toString())
        .find((it) => it === userId.toString())
        .value();
      if (didDown) {
        const {
          value: { [withUpDown.down]: downList },
        } = await db
          .collection(Model.collection)
          .findOneAndUpdate(
            { _id },
            { $pull: { [withUpDown.down]: userId } },
            { projection: { [withUpDown.down]: 1 }, returnOriginal: false }
          );
        return {
          [withUpDown.down]: {
            count: _.size(downList),
            used: false,
          },
        };
      }
      const {
        value: { [withUpDown.down]: downList, [withUpDown.up]: upList },
      } = await db.collection(Model.collection).findOneAndUpdate(
        { _id },
        {
          $push: { [withUpDown.down]: userId },
          $pull: { [withUpDown.up]: userId },
        },
        {
          projection: { [withUpDown.down]: 1, [withUpDown.up]: 1 },
          returnOriginal: false,
        }
      );
      return {
        [withUpDown.down]: {
          count: _.size(downList),
          used: true,
        },
        [withUpDown.up]: {
          count: _.size(upList),
          used: false,
        },
      };
    }

    const {
      value: { [withUpDown.down]: downList },
    } = await db.collection(Model.collection).findOneAndUpdate(
      { _id },
      { $push: { [withUpDown.down]: userId } },
      {
        projection: { [withUpDown.down]: 1 },
        returnOriginal: false,
      }
    );
    return {
      [withUpDown.down]: {
        count: _.size(downList),
        used: true,
      },
    };
  };

  this.getStatus = async ({ _id, userId }) => {
    _id = ObjectId(_id);

    const db = await dbProvider.get();
    const component = await db.collection(Model.collection).findOne(
      {
        _id,
      },
      { projection: { [withUpDown.up]: 1, [withUpDown.down]: 1 } }
    );

    if (component) {
      const {
        [withUpDown.down]: downList,
        [withUpDown.up]: upList,
      } = component;
      let didUp, didDown;
      if (userId) {
        didUp =
          _.chain(component)
            .get(`${withUpDown.up}`, [])
            .findIndex((it) => it.toString() === userId)
            .value() > -1;
        didDown =
          _.chain(component)
            .get(`${withUpDown.down}`, [])
            .findIndex((it) => it.toString() === userId)
            .value() > -1;
      } else {
        didUp = false;
        didDown = false;
      }
      return {
        [withUpDown.up]: {
          count: _.size(upList),
          used: didUp,
        },
        [withUpDown.down]: {
          count: _.size(downList),
          used: didDown,
        },
      };
    }
    return {
      [withUpDown.down]: {
        count: 0,
        used: false,
      },
      [withUpDown.up]: {
        count: 0,
        used: false,
      },
    };
  };

  this.getList = async ({ _id, upOrDown }) => {
    _id = ObjectId(_id);

    const db = await dbProvider.get();
    const component = await db.collection(Model.collection).findOne(
      {
        _id,
      },
      { projection: { [withUpDown[upOrDown]]: 1 } }
    );

    if (component && _.has(component, `${withUpDown[upOrDown]}`)) {
      return component[withUpDown[upOrDown]];
    }

    return [];
  };

  this.getMostRelevantFromIds = async (ids) => {
    ids = ids.map(ObjectId);

    const db = await dbProvider.get();
    return await db
      .collection(Model.collection)
      .find(
        {
          _id: { $in: ids }
        },
        { projection: { _id: 1 } }
      )
      .sort({ [withUpDown.up]: -1 })
      .limit(2)
      .toArray();
  };
};

const _ = require("lodash");
const createError = require("http-errors");
const dbProvider =  require("../db");
const Promise = require("bluebird");
const { ObjectId } = require("mongodb");
const idValidator = require("../services/idValidator");

const identityTransformer = {
  out: async (_, _object) => await _object,
};

const getObjectRenderDefaultFields = ({ fields, data }) => {
  return _(fields).difference(_.keys(data)).value();
};

const applyTransformer = async (req, { Model, data, fields }) => {
  const transformer = Model.transformer || identityTransformer;
  const renderDefaultFields = getObjectRenderDefaultFields({
    fields,
    data,
  });

  if (!_.size(renderDefaultFields)) {
    return await transformer.out(req, data);
  }

  for (const key of renderDefaultFields) {
    data[key] = _.get(Model, `fields.${key}`, null);
  }

  return _.omitBy(await transformer.out(req, data), _.isNull);
};

const serialize = (module.exports = async (
  req,
  { Model, query = {}, options = {} }
) => {
  if (!Model) {
    throw new createError.InternalServerError("invalid model");
  }

  let { condition, projection, sort, offset, limit } = query;

  condition = condition || {};
  projection = projection || {};
  sort = {
    ...(sort || {}),
    createdAt: -1,
  };

  let { group, refs } = options;

  group = group || "simple";
  refs = refs || null;

  const renderGroup = _.get(Model, `render.${group}`, null);

  if (!renderGroup) {
    return {};
  }

  for (field of renderGroup.fields) {
    projection = {
      ...projection,
      [field]: 1,
    };
  }

  const db = await dbProvider.get();

  let items = null;
  let fetch = null;

  if (!refs) {
    fetch = db
      .collection(Model.collection)
      .find(condition, { projection })
      .sort(sort);
  }

  if (_.isArray(refs)) {
    fetch = db
      .collection(Model.collection)
      .find(
        { ...condition, _id: { $in: _.map(refs, ObjectId) } },
        { projection }
      )
      .sort(sort);
  }

  if (ObjectId.isValid(refs)) {
    items = await db
      .collection(Model.collection)
      .findOne({ ...condition, _id: ObjectId(refs) }, { projection });
  }

  if (offset) {
    fetch = fetch.skip(parseInt(offset));
  }
  if (limit) {
    fetch = fetch.limit(parseInt(limit));
  }

  if (!items) {
    items = await fetch.toArray();
  }

  const render = await Promise.map(
    _.isArray(items) ? items : [items],
    async (item) => {
      //serialize simple object
      if (!renderGroup.refs) {
        return await applyTransformer(req, {
          Model,
          fields: renderGroup.fields,
          data: item,
        });
      }
      // serialize each mongo object
      for (const _object of renderGroup.refs || []) {
        if (idValidator.validateOneOrMany(item[_object.name])) {
          item[_object.name] = await serialize(req, {
            Model: Model.refs[_object.name],
            options: { group: _object.group, refs: item[_object.name] },
          });
        }
      }

      // transform current object
      return await applyTransformer(req, {
        Model,
        fields: renderGroup.fields,
        data: item,
      });
    }
  );

  return _.isArray(items) ? render : _.head(render);
});

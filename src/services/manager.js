const _ = require("lodash");
const dbProvider =  require("../db");
const Promise = require("bluebird");

exports.deleteMany = async ({ collection, conditions, ids }) => {
  const db = await dbProvider.get();

  if (_.size(ids) === 0) {
    return null;
  }

  return await db
    .collection(collection)
    .deleteMany(conditions, { _id: { $in: ids } });
};

exports.updateEach = async ({ Model, $setConditions }) => {
  const db = await dbProvider.get();
  return await Promise.map($setConditions, async ($setCondition) => {
    const { condition, $set } = $setCondition;
    await db
      .collection(Model.collection)
      .updateOne(condition, { $set });
  });
};

exports.insertMany = async ({ Model, values }) => {
  let newIds = {};
  if (_.size(values)) {
    const db = await dbProvider.get();
    ({ insertedIds: newIds } = await db
      .collection(Model.collection)
      .insertMany(values));
  }

  return Object.values(newIds);
};

exports.fetchMap = async (fetch) =>
  await Promise.map(
    fetch,
    async ({ Model, condition, projection }) =>
      await (await dbProvider.get())
        .collection(Model.collection)
        .find(condition, { projection: projection || {} })
        .toArray()
  );

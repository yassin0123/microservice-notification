const _ = require("lodash");
const dbProvider =  require("../db");
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");
const createError = require("http-errors");

module.exports = {
  validateOneOrMany: (ids) => {
    if (!_.isArray(ids)) {
      return ObjectID.isValid(ids);
    }

    for (const id of ids) {
      if (!ObjectID.isValid(id)) {
        return false;
      }
    }
    return true;
  },
  validateManyAsync: async (validations) => {
    if (_.isEmpty(validations)) {
      return;
    }
    for (const id of _(validations).map("ids").flattenDeep().value()) {
      if (!ObjectID.isValid(id)) {
        throw createError.BadRequest(`${id} is invalid`);
      }
    }
    const db = await dbProvider.get();
    await Promise.each(validations, async ({ Model, ids }) => {
      const mongoIds = ids.map((id) => new ObjectID(id));
      const count = await db
        .collection(Model.collection)
        .countDocuments({ _id: { $in: mongoIds } });
      if (count !== _.size(mongoIds)) {
        throw createError.BadRequest(
          `id ${Model.collection.toLowerCase()} doesn't exists`
        );
      }
    });
  },
  validateRouterId: ({ Model, key }) => async (req, res, next) => {
    if (!Model) {
      throw createError.InternalServerError(`invalid Model`);
    }

    key = key || "_id";
    const id = req.params[key];

    if (!ObjectID.isValid(id)) {
      throw createError.BadRequest(`${id} is invalid`);
    }

    const db = await dbProvider.get();
    const count = await db
      .collection(Model.collection)
      .countDocuments({ _id: new ObjectID(id) });
    if (count === 0) {
      throw createError.BadRequest(
        `id ${Model.collection.toLowerCase()} doesn't exists`
      );
    }

    return next();
  },
};

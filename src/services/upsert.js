const _ = require("lodash");
const createError = require("http-errors");
const dbProvider =  require("../db");
const Promise = require("bluebird");
const { validateOneOrMany } = require("./idValidator");
const { ObjectId } = require("mongodb");
const { DateTime } = require("luxon");

//can be optimized by insertMany
const upsert = (module.exports = async ({ Model, payload }) => {
  if (_.isArray(payload)) {
    return await Promise.map(
      payload,
      async (it) => await upsert({ Model, payload: it })
    );
  }

  const refs = _.chain(Model.refs).keys().intersection(_.keys(payload)).value();

  await Promise.each(refs, async (ref) => {
    if (!validateOneOrMany(payload[ref])) {
      payload[ref] = await upsert({
        Model: Model.refs[ref],
        payload: payload[ref],
      });
    }
  });

  const db = await dbProvider.get();
  let query = db.collection(Model.collection);

  if (!_.has(payload, "_id")) {
    const { insertedId } = await query.insertOne({
      ...payload,
      createdAt: DateTime.local(),
    });
    return insertedId;
  }

  const _id = ObjectId(payload._id);
  payload = {
    ...payload,
    lastUpdatedAt: DateTime.local(),
  };
  const updateQuery = _.get(payload, "$daoConfig.addToSet", false)
    ? { $addToSet: _.omit(payload, ["_id", "$daoConfig"]) }
    : { $set: _.omit(payload, ["_id", "$daoConfig"]) };

  await query.updateOne({ _id }, updateQuery);
  return _id;
});

const _ = require("lodash");
const { DateTime } = require("luxon");

module.exports = (dateTime) => {
  const now = DateTime.local();
  let delayObject = now
    .diff(DateTime.fromJSDate(dateTime), [
      "months",
      "days",
      "hours",
      "minutes",
      "seconds",
    ])
    .toObject();
  return (
    _(delayObject)
      .pick(["months", "days", "hours", "minutes", "seconds"])
      .pickBy((el) => el !== 0)
      .map((value, key) => `${Math.floor(value)} ${key}`)
      .splice(0, 1)
      .join() || null
  );
};

var express = require("express");
const user = require("../middleware/user");

const auth = require("./auth");
const notification = require("./notification");

module.exports = express
  .Router()
  .use(user.check)
  .use("/auth", auth)
  .use("/notifications", notification);

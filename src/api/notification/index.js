const express = require("express");
const Model = require("../../models/notification");

const user = require("../../middleware/user");
const service = require("./service");
const render = require("../../middleware/render");

module.exports = express
  .Router()
  .get("/", user.isUser, service.getNotifications, render.get);
const createError = require("http-errors");
const dbProvider = require("../../db");
const Promise = require("bluebird");
const { ObjectId } = require("mongodb");
const Model = require("../../models/notification");

exports.getNotifications = async (req, res, next) => {
  const db = await dbProvider.get();
  const notifications = await db
    .collection(Model.collection)
    .findOne(
      { user: ObjectId(req.auth._id) },
      { data: [req.query.offset, req.query.limit] }
    );

  req.render = notifications.data;

  return next();
};

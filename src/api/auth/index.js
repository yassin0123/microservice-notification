const express = require("express");

const validate = require("./validate");
const dao = require("./dao");
const render = require("../../middleware/render");

module.exports = express
  .Router()
  .post("/register", validate.register, dao.create, render.create)
  .post("/login", validate.login, render.get);

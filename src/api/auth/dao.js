const _ = require("lodash");
const Model = require("../../models/user");
const modelEntity = require("../../models/entity");
const dbProvider =  require("../../db");
const bcrypt = require("bcrypt");
const { AUTH_SECRET } = require("../../config");
const jwt = require("jsonwebtoken");

module.exports = {
  create: async (req, res, next) => {
    const user = {
      ...req.body,
      password: bcrypt.hashSync(req.body.password, 8),
    };
    const db = await dbProvider.get();
    await db.collection(Model.collection).insertOne(user);
    await db
      .collection(modelEntity.collection)
      .insertOne({ user: user._id });

    const token = jwt.sign(
      {
        _id: user._id.toString(),
      },
      AUTH_SECRET,
      {
        expiresIn: "48h",
      }
    );

    req.render = { token };

    return next();
  },
};

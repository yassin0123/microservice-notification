const _ = require("lodash");
const Model = require("../../models/user");
const dbProvider =  require("../../db");
const bcrypt = require("bcrypt");
const { AUTH_SECRET } = require("../../config");
const jwt = require("jsonwebtoken");
const createHttpError = require("http-errors");

exports.register = async (req, res, next) => {
  const { error } = await Model.schema.register.validate(req.body);
  if (error) {
    throw new createHttpError.BadRequest(error.message);
  }

  const { username, email } = req.body;
  const db = await dbProvider.get();
  const duplicate = await db
    .collection(Model.collection)
    .findOne({ $or: [{ username }, { email }] }, { projection: { _id: 1 } });

  if (duplicate) {
    throw new createHttpError.BadRequest("username or email already exists");
  }

  return next();
};

exports.login = async (req, res, next) => {
  const { error } = await Model.schema.login.validate(req.body, {});
  if (error) {
    throw new createHttpError.BadRequest(error.message);
  }

  const { username, password } = req.body;
  const db = await dbProvider.get();
  const exists = await db
    .collection(Model.collection)
    .findOne({ username }, { projection: { _id: 1, password: 1 } });

  if (!exists) {
    throw new createHttpError.BadRequest("invalid username or password");
  }

  if (!bcrypt.compareSync(password, exists.password)) {
    throw new createHttpError.BadRequest("invalid password");
  }

  const token = jwt.sign(
    {
      _id: exists._id.toString(),
    },
    AUTH_SECRET,
    {
      expiresIn: "48h",
    }
  );

  req.render = { token };

  return next();
};

const _ = require("lodash");
const { name: appName } = require("../../package.json");
const config = require("../config");
const pino = require("pino");
const Joi = require("@hapi/joi");
const {
  SCHEMA: { PAGINATION },
  DEFAULT_PAGINATION_LIMIT,
} = require("../constant");

exports.logger = pino({ level: config.LOG_LEVEL }).child({ appName });

exports.withPagination = (schema = {}) => ({
  ...schema,
  route: {
    ...(schema.route || {}),
    all: Joi.object({
      ..._.get(schema, "route.all", {}),
      ...PAGINATION,
    }).default({ limit: DEFAULT_PAGINATION_LIMIT }),
  },
});

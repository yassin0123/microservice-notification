const Joi = require("joi");
Joi.objectId = require("joi-objectid");
const { withPagination } = require("../../helper");

module.exports = {
  collection: "Notification",
  schema: withPagination(),
  render: {
    simple: {
      fields: ["user", "createdAt", "type", "data"],
      refs: [
        {
          name: "user",
          group: "username_avatar",
        },
      ],
    },
    simple: {
      fields: ["user", "createdAt", "type", "data"],
      refs: [
        {
          name: "user",
          group: "username_avatar",
        },
      ],
    },
  },
  extras: {
    type: {
      USER_STREAM_STATUS: "USER_STREAM_STATUS",
    },
  },
  refs: {
    user: require("../user"),
  },
};

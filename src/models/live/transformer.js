const _ = require("lodash");
const delay = require("../../services/delay");

const computeDelays = (req, response) => {
  let transformed = {
    ...response,
    delay: delay(response.createdAt),
  };

  if (response.type === "IN_DANGER") {
    if (req.params._id) {
      transformed = {
        ...transformed,
        inDanger: {
          ...response.inDanger,
          locations: _.map(response.inDanger.locations, (it) => ({
            ...it,
            delay: delay(it.createdAt),
          })),
        },
      };
    } else if (response.inDanger.locations) {
      const lastLocation = _.head(response.inDanger.locations);
      transformed = {
        ...transformed,
        inDanger: {
          ..._.omit(response.inDanger, "locations"),
          lastLocation: {
            ...lastLocation,
            delay: delay(lastLocation.createdAt),
          },
        },
      };
    }
  }

  return transformed;
};

module.exports = {
  out: computeDelays,
};

const Joi = require("@hapi/joi");
Joi.ObjectId = require("joi-objectid")(Joi);
const { withPagination } = require("../../helper");

module.exports = {
  upsert: Joi.alternatives([
    {
      privacy: Joi.string().valid("PRIVATE", "PUBLIC").required(),
      type: Joi.string().valid("IN_DANGER", "EMERGENCY", "OTHER").required(),
      geolocation: {
        latitude: Joi.number().positive().required(),
        longitude: Joi.number().positive().required(),
      },
    },
  ]),
  patch: Joi.alternatives([
    {
      inDanger: Joi.alternatives([
        {
          geolocation: {
            latitude: Joi.number().positive().required(),
            longitude: Joi.number().positive().required(),
          },
        },
        {
          status: Joi.string().valid("IN_DANGER", "SAFE"),
        },
      ]),
    },
    {
      stream: Joi.alternatives([
        {
          viewed: Joi.bool().strict().valid(true).required(),
        },
        {
          viewing: Joi.bool().strict().required(),
        },
      ]),
    },
    {
      privacy: Joi.string().valid("PRIVATE", "PUBLIC"),
    },
    {
      status: Joi.string().valid("ACCEPTED", "REJECTED"),
    },
  ]),
  route: {
    all: Joi.alternatives([
      {
        user: Joi.ObjectId(),
        queryType: Joi.string().valid("WALL").default("WALL"),
      },
      Joi.object({
        type: Joi.string().valid("IN_DANGER", "EMERGENCY", "OTHER"),
        privacy: Joi.string().valid("PRIVATE", "PUBLIC"),
        status: Joi.string().valid("ACCEPTED", "REJECTED", "PENDING"),
        user: Joi.ObjectId(),
        limit: Joi.number().integer().positive().default(10),
        offset: Joi.number().integer().positive().allow(0),
        queryType: Joi.string().valid("LIST").default("LIST"),
      }).xor("type", "privacy", "status"),
    ]),
  },
};

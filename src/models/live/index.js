const schema = require("./schema");
const transformer = require("./transformer");

module.exports = {
  collection: "StreamEvent",
  schema,
  transformer,
  render: {
    simple: {
      fields: [
        "user",
        "createdAt",
        "inDanger",
        "type",
        "stream",
        "status",
        "privacy",
      ],
      refs: [
        {
          name: "user",
          group: "username_avatar",
        },
      ],
    },
    details: {
      fields: [
        "user",
        "createdAt",
        "inDanger",
        "type",
        "stream",
        "status",
        "privacy",
      ],
      refs: [
        {
          name: "user",
          group: "username_avatar",
        },
      ],
    },
  },
  extras: {
    privacy: {
      PRIVATE: "PRIVATE",
      PUBLIC: "PUBLIC",
    },
    status: {
      PENDING: "PENDING",
      ACCEPTED: "ACCEPTED",
      REJECTED: "REJECTED,",
    },
    stream: {
      status: {
        NONE: "NONE",
        STREAMING: "STREAMING",
        STREAMING_DONE: "STREAMING_DONE",
      },
      type: {
        STATIC: "STATIC",
        STREAM: "STREAM",
      },
    },
    type: {
      IN_DANGER: "IN_DANGER",
      EMERGENCY: "EMERGENCY",
      OTHER: "OTHER",
    },
    inDanger: {
      status: {
        IN_DANGER: "IN_DANGER",
        SAFE: "SAFE",
      },
    },
    wall: {
      LIMIT_TRENDING: 8,
      LIMIT_PER_TYPE: 8,
    },
  },
  refs: {
    user: require("../user"),
  },
};

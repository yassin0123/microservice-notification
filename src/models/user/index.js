const schema = require("./schema");

module.exports = {
  collection: "User",
  schema,
  render: {
    username_only: {
      fields: ["username"],
    },
    username_avatar: {
      fields: ["username", "avatar", "entity"],
    },
    simple: {
      fields: ["username", "name", "age", "entity"],
    },
    infos: {
      fields: ["username", "name", "age", "email", "entity"],
    },
  },
};

const Joi = require("@hapi/joi");

module.exports = {
  register: Joi.object({
    name: Joi.string(),
    age: Joi.number().integer().positive(),
    username: Joi.string()
      .required()
      .error((errors) => new Error("username missing")),
    email: Joi.string()
      .email()
      .required()
      .error((errors) => new Error("email missing")),
    password: Joi.string()
      .error((errors) => new Error("password missing"))
      .required(),
    confirmPassword: Joi.ref("password"),
  }),
  login: Joi.object({
    username: Joi.string()
      .required()
      .error((errors) => new Error("username missing")),
    password: Joi.string()
      .required()
      .error((errors) => new Error("password missing")),
  }),
  update: Joi.object({
    username: Joi.string()
      .required()
      .error((errors) => new Error("username missing")),
    password: Joi.string().error((errors) => new Error("password missing")),
    confirmPassword: Joi.ref("password"),
    avatar: Joi.string()
      .base64()
      .error((errors) => new Error("avatar missing")),
  }),
};


module.exports = {
    getMissingKeyMessage: (key)=>`${key} is missing`,
    getInvalidIdMessage: (id)=>`${id} is invalid`,
    getNonExistantIdMessage: (id)=>`${id} is doesn't exist`
};
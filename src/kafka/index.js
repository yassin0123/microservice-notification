const { Kafka } = require("kafkajs");
const {
  EVENTS: { ON_POST_PUBLISH, ON_DONE_PUBLISH },
} = require("../constant");

const { onPostPublish, onDonePublish } = require("./handler");

const kafka = new Kafka({
  clientId: "microservice.live",
  brokers: ["kafka:29092"],
});

const consumer = kafka.consumer({ groupId: "test-group" });

exports.listen = async () => {
  await consumer.connect();
  await consumer.subscribe({ topic: "live", fromBeginning: false });

  console.log("listening in kafka consumer");

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      const { event, data } = JSON.parse(message.value.toString());
      switch (event) {
        case ON_POST_PUBLISH:
          onPostPublish(data);
          break;
        case ON_DONE_PUBLISH:
          onDonePublish(data);
          break;
        default:
          break;
      }
    },
  });
};

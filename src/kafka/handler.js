const _ = require("lodash");
const dbProvider = require("../db");
const {
  LIVE_STATUS: { STREAMING, STREAMING_DONE },
  STREAMING_PREFIX,
  STREAMING_STATIC_PREFIX,
} = require("../constant");
const Model = require("../models/live");

exports.onPostPublish = async (data) => {
  const { streamPath, staticFileName } = data;
  const streamKey = _.chain(streamPath.substring(1)).split("/").last().value();
  const url = `${STREAMING_PREFIX}/${streamKey}.flv`;
  const staticURl = `${STREAMING_STATIC_PREFIX}/${streamKey}/${staticFileName}`;
  console.log("revceived POST_PUBLISH  of", data, streamKey);
  const db = await dbProvider.get();
  await db.collection(Model.collection).updateOne(
    { "stream.key": streamKey },
    {
      $set: {
        "stream.status": STREAMING,
        "stream.static": staticURl,
        "stream.url": url,
      },
    }
  );
};

// needs refacto replace 3 queries with 1 aggregate
exports.onDonePublish = async (data) => {
  const streamKey = _.chain(data.substring(1)).split("/").last().value();
  const url = `${STREAMING_PREFIX}/${streamKey}.flv`;
  console.log("revceived DONE_PUBLISH  of", data, streamKey);
  const db = await dbProvider.get();
  const streamEvent = await db
    .collection(Model.collection)
    .findOne({ "stream.key": streamKey });

  await db.collection(Model.collection).updateOne(
    { "stream.key": streamKey },
    {
      $set: {
        "stream.status": STREAMING_DONE,
        "stream.url": streamEvent.stream.static,
      },
    }
  );
  await db.collection(Model.collection).updateOne(
    { "stream.key": streamKey },
    {
      $unset: {
        "stream.static": "",
      },
    }
  );
};

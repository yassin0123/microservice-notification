FROM node:12-alpine

# Create app directory
WORKDIR /app

ENV PORT=8080
#Expose port 8080
EXPOSE $PORT

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

COPY ./src ./src


CMD [ "npm", "start" ]
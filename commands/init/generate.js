const _ = require("lodash");
const { ObjectId } = require("mongodb");
const bcrypt = require("bcrypt");
const { v4: uuid } = require("uuid");
const fs = require("fs");
const ModelUser = require("../../src/models/user");
const ModelLive = require("../../src/models/live");

const {
  privacy: { PUBLIC, PRIVATE },
  status: { ACCEPTED, REJECTED, PENDING },
  type: { OTHER, IN_DANGER },
  stream: {
    type: { STATIC, STREAM },
    status: { STREAMING },
  },
} = ModelLive.extras;

const generatorConfig = {
  STATIC_STREAMS_PER_USER: 5,
  STREAMINGS_PER_USER: 5,
  PRIVATE_PER_USER: 5,
  PENDING_PER_USER: 5,
  IN_DANGER_PER_USER: 2,
};

const AVATAR = '/cloud/public/storage/files/user/user_avatar.jpg';
const STATIC_STREAM_URL = "/cloud/public/storage/streams/dance.mp4";
const THUMBNAIL = '/cloud/public/storage/files/social/image/34fe3c20-8464-46f6-8ac0-7fb02cfab850.jpeg';

const generateUser = (username) => {
  return {
    _id: ObjectId().toString(),
    firstName: `${username} firstname`,
    lastName: `${username} lastname`,
    username,
    avatar: AVATAR,
    email: `${username}@gmail.com`,
    password: bcrypt.hashSync(username, 8),
  };
};

const generateStaticSimpleStream = (params = {}) => {
  const {
    privacy = PUBLIC,
    status = ACCEPTED,
    stream: { title, views } = { title: "something happened", views: 50 },
  } = params;
  return {
    _id: ObjectId().toString(),
    privacy,
    status,
    type: OTHER,
    stream: {
      title,
      description: "lorem ipsup stuff",
      thumbnail:THUMBNAIL,
      views,
      type: STATIC,
      url: STATIC_STREAM_URL,
    },
  };
};

const generateSimpleStream = (params = {}) => {
  const {
    privacy = PUBLIC,
    status = ACCEPTED,
    stream: { title, status: streamStatus = STREAMING, views } = {
      title: "something happened",
      views: 50,
      status: STREAMING,
    },
  } = params;
  const key = uuid();
  return {
    _id: ObjectId().toString(),
    privacy,
    type: OTHER,
    status,
    stream: {
      title,
      description: "look at this , this is new paalll",
      thumbnail:THUMBNAIL,
      views,
      viewing: [],
      status: streamStatus,
      type: STREAM,
      key,
      url: `/cloud/public/live/${key}.flv`,
    },
  };
};

const generateInDangerStream = (params = {}) => {
  const {
    privacy = PUBLIC,
    status = ACCEPTED,
    stream: { title, views } = {
      title: "something happened",
      views: 50,
      status: STATIC,
    },
  } = params;

  return {
    _id: ObjectId().toString(),
    privacy,
    status,
    type: IN_DANGER,
    stream: {
      title,
      description: "look at this lorep ipsum",
      thumbnail:THUMBNAIL,
      views,
      type: STATIC,
      url: STATIC_STREAM_URL,
    },
    inDanger: {
      status: IN_DANGER,
      locations: [
        {
          geolocation: {
            longitude: 10.3328442,
            latitude: 36.7296502,
          },
          createdAt: "2020-12-13T09:05:07.042Z",
        },
        {
          geolocation: {
            longitude: 10.3328442,
            latitude: 36.7296502,
          },
          createdAt: "2020-12-13T09:05:13.972Z",
        },
        {
          geolocation: {
            longitude: 10.3328442,
            latitude: 36.7296502,
          },
          createdAt: "2020-12-13T09:05:20.506Z",
        },
      ],
    },
  };
};

const appendUserStreams = (user, streams) => {
  return _.map(streams, (stream) => ({
    user: user._id,
    ...stream,
  }));
};

const generate = () => {
  console.log("generating payload ...");
  const {
    STATIC_STREAMS_PER_USER,
    STREAMINGS_PER_USER,
    PRIVATE_PER_USER,
    PENDING_PER_USER,
    IN_DANGER_PER_USER,
  } = generatorConfig;

  const users = _.times(10, (index) => generateUser(`user${index + 1}`));
  const streams = _.map(users, (user) => {
    const staticStreams = _.times(
      STATIC_STREAMS_PER_USER,
      generateStaticSimpleStream
    );
    const streams = _.times(STREAMINGS_PER_USER, generateSimpleStream);
    const privateStaticStreams = _.times(PRIVATE_PER_USER, (index) =>
      generateStaticSimpleStream({
        privacy: PRIVATE,
        stream: { title: `private stream ${index + 1} for ${user.username}` },
      })
    );
    const pendingStreams = _.times(PENDING_PER_USER, (index) =>
      generateStaticSimpleStream({
        status: PENDING,
        stream: { title: `pending stream ${index + 1} for ${user.username}` },
      })
    );

    const inDangerStreams = _.times(IN_DANGER_PER_USER, (index) =>
      generateInDangerStream({
        stream: { title: `${user.username} is in danger number ${index + 1}` },
      })
    );

    return appendUserStreams(user, [
      ...staticStreams,
      ...streams,
      ...privateStaticStreams,
      ...pendingStreams,
      ...inDangerStreams,
    ]);
  });

  const payload = {
    [ModelUser.collection]: users,
    [ModelLive.collection]: _.flatten(streams),
  };

  fs.writeFileSync("./payload.json", JSON.stringify(payload));
  console.log('generated payload');
};

generate();

const _ = require("lodash");
const { ObjectId } = require("mongodb");
const Promise = require("bluebird");
const dbProvider =  require("../../src/db");
const { DateTime } = require("luxon");
const payload = require("../../payload.json");

const refs = {
  StreamEvent: ["user"],
};

const transformer = (collection, object) => {
  if (collection !== "StreamEvent") {
    return object;
  }

  return object.type === "IN_DANGER"
    ? {
        ...object,
        inDanger: {
          ...object.inDanger,
          locations: _.map(object.inDanger.locations, (it) => ({
            ...it,
            createdAt: DateTime.local(),
          })),
        },
      }
    : object;
};

const objectify = () => {
  const objectified = _.cloneDeep(payload);
  for (const collection of _.keys(objectified)) {
    for (const item of objectified[collection]) {
      item._id = ObjectId(item._id);
      item.createdAt = DateTime.local();
      const collectionRefs = _.intersection(_.keys(item), refs[collection]);
      if (!_.size(collectionRefs)) {
        continue;
      }
      for (const ref of collectionRefs) {
        item[ref] = _.isArray(item[ref])
          ? _.map(item[ref], ObjectId)
          : ObjectId(item[ref]);
      }
    }
  }

  return objectified;
};

const init = async () => {
  console.log("dropping database ...");
  const db = await dbProvider.get();
  await db.dropDatabase();
  console.log("database dropped");

  const collections = _.keys(payload);
  const objectified = objectify();

  await Promise.each(collections, async (collection) => {
    console.log("populating ", collection, "found ", _.size(objectified[collection]),  " items ...");
    const items = _.map(objectified[collection], (it) =>
      transformer(collection, it)
    );
    await db.collection(collection).insertMany(items);
  });

  console.log("done. XD ");
  process.exit(0);
};

init();
